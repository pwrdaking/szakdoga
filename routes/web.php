<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/test', 'HomeController@test')->name('test');

Route::get('/', 'GuestController@index')->name('index');
Route::get('/home', 'HomeController@index')->name('home');

//Teacher
Route::get('/teacher', 'TeacherController@index')->name('teacher');
Route::get('/teacher/register', 'TeacherController@register')->name('teacher.register');
Route::delete('/teacher/delete/{id}', 'TeacherController@destroy')->name('teacher.destroy');

Route::get('/profile', 'ProfileController@edit')->name('profile');
Route::post('/profile', 'ProfileController@update');
Route::resource('/news', 'NewsController');
Route::post('/child/confirm/{id}', 'GroupController@addChildren')->name('child.confirm');
Route::delete('/child/remove', 'GroupController@removeChildren')->name('child.remove');
Route::resource('/child', 'ChildController');
Route::resource('/group', 'GroupController');
Route::resource('/plan', 'PlanController');
Route::resource('/meal', 'MealController');
Route::resource('/product', 'ProductController');
Route::resource('/message', 'MessageController', ['only' => ['store', 'destroy']]);
Route::get('/diary', 'DiaryController@index')->name('diary.home');
Route::resource('/absence', 'AbsenceController');

Route::get('/cart', 'CartController@index');
Route::post('cart/add', 'CartController@add')->name('cart.add');
Route::delete('cart/{id}/delete', 'CartController@destroy')->name('cart.destroy');
Route::delete('cart/delete/all', 'CartController@deleteAll')->name('cart.destroy.all');
Route::post('/checkout', 'OrderController@checkout')->name('checkout');
Route::get('/payment-success', 'OrderController@paymentSuccess')->name('payment.success');
Route::get('/invoice', 'OrderController@invoice')->name('invoice');
Route::delete('subscription/stop/{id}', 'ProductController@stopSubscription')->name('subscription.stop');
