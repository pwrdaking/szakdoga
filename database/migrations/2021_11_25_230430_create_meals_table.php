<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {
            $table->id();
            $table->integer('week');
            $table->string('monday_breakfast');
            $table->string('monday_lunch');
            $table->string('monday_dinner');
            $table->string('tuesday_breakfast');
            $table->string('tuesday_lunch');
            $table->string('tuesday_dinner');
            $table->string('wednessday_breakfast');
            $table->string('wednessday_lunch');
            $table->string('wednessday_dinner');
            $table->string('thursday_breakfast');
            $table->string('thursday_lunch');
            $table->string('thursday_dinner');
            $table->string('friday_breakfast');
            $table->string('friday_lunch');
            $table->string('friday_dinner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meals');
    }
}
