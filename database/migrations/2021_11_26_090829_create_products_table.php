<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{

    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->integer('validity_count');
            $table->integer('price');
            $table->integer('type');
            $table->timestamps();
        }) ;
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
