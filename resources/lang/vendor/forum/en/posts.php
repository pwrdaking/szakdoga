<?php

return [

    'actions' => 'Kérdés műveletek',
    'created' => 'Kérdés létrehozva',
    'delete' => 'Kérdés törlése|Kérdések törlése',
    'deleted' => 'Kérdés törölve|Kérdések törölve',
    'edit' => 'Kérdés szerkesztése',
    'last' => 'Útolsó kérdés',
    'perma_deleted' => 'Kérdés végleges törlése|Kérdések végleges törlése',
    'post' => 'Kérdés|Kérdések',
    'restore' => 'Kérdése visszaállítása|Kérdések visszaállítása',
    'restored' => 'Kérdés visszaállítva|Kérdések visszaállítva',
    'select_all' => 'Összes kérdés kijelölése',
    'updated' => 'Kérdés frissítve|Kérdések frissítve',
    'view' => 'Kérdés megtekintése',
    'your_post' => 'Te kérdésed',

];
