@extends('layouts.app')

@section('content')

<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">Új hiányzás</div>

                <div class="card-body">
                    @if(isset($errorMessage))
                        <p class="text-red">
                            {{ $errorMessage }}
                        </p>
                    @endif

                    <form method="POST" action="{{ route('absence.update', $absence->id) }}">
                        @csrf
                        {{ method_field("PUT") }}

                        <div class="form-group row">
                            <label for="start" class="col-md-3 col-form-label text-md-right">Kezdete</label>

                            <div class="col-md-6">
                                <input id="start" type="date" class="form-control @error('start') is-invalid @enderror" name="start" value="{{ $absence->start }}" autofocus
                                aria-describedby="birthday" required
                                min="2018-01-01">
                                @error('start')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="end" class="col-md-3 col-form-label text-md-right">Vége</label>

                            <div class="col-md-6">
                                <input id="end" type="date" class="form-control @error('end') is-invalid @enderror" name="end" value="{{ $absence->end }}" autofocus
                                aria-describedby="end" required
                                min="2021-11-25">
                                @error('end')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cause" class="col-md-3 col-form-label text-md-right">Ok</label>

                            <div class="col-md-6">
                                <input id="cause" type="text" class="form-control @error('cause') is-invalid @enderror" name="cause" value="{{ $absence->cause }}" autocomplete="cause" placeholder="Nátha" autofocus>

                                @error('cause')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <input type="hidden" id="" name="child_id" value="{{ app('request')->input('child_id') ?? "" }}">
                        <div class="form-group row justify-content-center text-center mt-3 mb-0">
                            <button type="submit" class="btn btn-primary" id="btn-save">Mentés</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
