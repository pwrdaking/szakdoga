@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-8 mt-2">
        <h2>Gyerekek</h2>

        <a href="{{ route('child.create') }}" class="btn btn-warning float-right mb-1">Új gyerek beíratása</a>

        @if(count($children) > 0)
            <table class="table table-bordered">
                <tr>
                    <td>Név</td>
                    <td>Érzékenység</td>
                    <td>Alapbetegség</td>
                    <td>Születésnap</td>
                    <td>Csoport</td>
                </tr>
                @foreach($children as $child)
                    <tr>
                        <td>
                            <a href="/child/{{$child->id}}?from=child" >{{ $child->child_name }}</a>
                        </td>
                        <td>{{ $child->sensitivity }}</td>
                        <td>{{ $child->base_disease }}</td>
                        <td>{{ $child->birthday }}</td>
                        <td> {{ $child->group  }} </td>
                        <td class="row mx-0 justify-content-center">
                            <a href="/child/{{$child->id}}?from=child" class="btn btn-success float-right mr-1 mb-1"><i class="fas fa-file-alt"></i></a>
                            <a href="{{ route('child.edit', $child->id) }}" class="btn btn-primary float-right mr-1 mb-1"><i class="fas fa-edit"></i></a>
                            <form action="{{ route('child.destroy', $child->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button onclick="return confirm('Biztos, hogy törölni szeretnéd {{ $child->child_name }}-t?');" type="submit" class="btn btn-danger mr-1"><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>

            @if(count($children) > 0)
                <div class="pagination">
                    <?php echo $children->render();  ?>
                </div>
            @endif

        @else
            <i>Nincs még beíratott gyereked</i>
        @endif

    </div>
</div>
@endsection
