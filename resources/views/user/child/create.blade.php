@extends('layouts.app')

@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">Új gyerek beíratása</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('child.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="child_name" class="col-md-3 col-form-label text-md-right">Név*</label>

                            <div class="col-md-6">
                                <input id="child_name" type="text" class="form-control @error('child_name') is-invalid @enderror" name="child_name" value="{{ old('child_name') }}" autocomplete="child_name" placeholder="Tóth Jancsika" autofocus required>

                                @error('child_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sensitivity" class="col-md-3 col-form-label text-md-right">Érzékenység</label>

                            <div class="col-md-6">
                                <input id="sensitivity" type="text" class="form-control @error('sensitivity') is-invalid @enderror" name="sensitivity" value="{{ old('sensitivity') }}" autocomplete="sensitivity" placeholder="Laktóz, glutén" autofocus>

                                @error('sensitivity')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="base_disease" class="col-md-3 col-form-label text-md-right">Alapbetegség</label>

                            <div class="col-md-6">
                                <input id="base_disease" type="text" class="form-control @error('base_disease') is-invalid @enderror" name="base_disease" value="{{ old('base_disease') }}" autocomplete="base_disease" placeholder="Hiperaktivitás, autizmus" autofocus>

                                @error('base_disease')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birthday" class="col-md-3 col-form-label text-md-right">Születésnap (min. 2018-01-01)</label>

                            <div class="col-md-6">
                                <input id="birthday" type="date" class="form-control @error('birthday') is-invalid @enderror" name="birthday" value="{{ old('birthday') }}" autofocus
                                value="2018-07-22" aria-describedby="birthday" required
                                min="2018-01-01">
                                @error('birthday')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center text-center mt-3 mb-0">
                            <button type="submit" class="btn btn-primary" id="btn-save">Mentés</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
