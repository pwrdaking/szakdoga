@extends('layouts.app')

@section('content')
    <div class="container pb-4 mt-3">
        <div class="row justify-content-between mb-3">
            <a href="/{{ app('request')->input('from') ?? "" }}" class="btn btn-primary mr-2">Vissza</a>
            @if(isset($child->base_subscription_until))
                <span class="text-success">
                    Bölcsödei alapdíj befizetve eddig: {{ $child->base_subscription_until }}.
                </span>
            @else
            <span class="text-danger">
                Bölcsödei alapdíj befizetése szükséges!
            </span>
            @endif
        </div>

        <div class="row justify-content-between mb-4">
            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-0">{{ $child->child_name }}</h4>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title">{{ $child->group_name }}</h5>

                      <p class="card-text mb-0">Érzékenység:</p>
                      <p class="ml-3">{{ $child->sensitivity }}</p>

                      <p class="card-text mb-0">Alapbetegség:</p>
                      <p class="ml-3">{{ $child->base_disease }}</p>

                      <p class="card-text mb-0">Kor:</p>
                      <p class="ml-3">{{ $child->months }} hónapos</p>

                      @if (isset($child->teacher->parent_name))
                        <p class="card-text mb-0">Tanár adatai:</p>
                        <p class="ml-3 mb-0">{{ $child->teacher->parent_name }}</p>
                        <p class="ml-3 mb-0">{{ $child->teacher->phone }}</p>
                        <p class="ml-3 mb-0">{{ $child->teacher->email }}</p>
                      @endif
                    </div>
                  </div>
            </div>

            <div class="col-6">
                <div class="card">
                    <div class="d-flex card-header justify-content-between">
                        <h4 class="mb-0">Hiányzások</h4>
                        <a href="/absence/create?child_id={{$child->id}}" class="btn btn-sm btn-primary float-right mr-1 mb-1">
                            Új hiányzás
                        </a>
                    </div>
                    <div class="card-body" style="max-height: 350px;overflow-y: scroll;">
                        @if (count($child->absences) > 0)
                            @foreach ($child->absences as $absence)
                            <div class="row">
                                    <div class="col-9">
                                        <p class="mb-0">({{ $absence->start }}) - ({{ $absence->end }})</p>
                                        <p class="mb-0">Ok: <br> {{ $absence->cause }}</p>
                                    </div>
                                    <div class="d-flex col-3 align-items-center">
                                        <div class="w-100">
                                            <a href="/absence/{{ $absence->id }}/edit?child_id={{$child->id}}" class="btn btn-primary float-right mr-1 mb-1">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <form action="{{ route('absence.destroy', $absence->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{method_field('DELETE')}}
                                                <button onclick="return confirm('Biztos, hogy törölni szeretnéd a hiányzást?');" type="submit" class="btn btn-danger mr-1"><i class="fas fa-trash"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-1">
                            @endforeach
                        @else
                                <h5>Nincs még hiányzása.</h5>
                        @endif
                    </div>
                </div>
            </div>

        </div>

        <div class="row justify-content-center pb-5">
            <div class="col-8">
                <div class="card">
                    <div class="d-flex card-header justify-content-between">
                        <h4 class="mb-0">Megjegyzések</h4>
                    </div>
                    <div id="message-container" class="card-body" style="min-height: 400px; max-height: 600px;overflow-y: scroll;">
                        @if(count($child->messages) > 0)
                            @foreach ($child->messages as $message)
                            <div class="card bg-primary mb-3 text-white">
                                <div class="card-body pt-3 pb-1">
                                    <div class="row mx-0">
                                        <div class="col-12">{{ $message->message }}</div>

                                    </div>
                                    <div class="row justify-content-end">
                                        <span class="text-message-date">
                                            {{ $message->created_at }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <h5>
                                Még nincsenek megjegyzések.
                            </h5>
                            <p>
                                A nevelő nő fog megjegyzéseket felvenni a gyermek fejlődéséről és fontosabb eseményekről vele kapcsolatban.
                            </p>
                        @endif

                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <div class="card" style="">
                    <div class="card-header">
                      Aktív egyéb szolgáltatás
                    </div>
                    <div style="max-height: 250px;overflow-y: scroll">
                        @if (count($activeSpecialSubscriptions) > 0)
                        <ul class="list-group list-group-flush">
                            @foreach ($activeSpecialSubscriptions as $sub)
                                <li class="list-group-item mx-0">
                                    <div class="row justiy-content-between">
                                        <div class="col-11">
                                            {{ $sub->product }} | <span class="text-success">eddig: {{ $sub->valid_until }}</span>
                                        </div>
                                        <div class="col-1">
                                            <form action="{{ route('subscription.stop', $sub->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{method_field('DELETE')}}

                                                <button onclick="return confirm('Biztos, hogy leszeretnéd mondani a szolgáltatást?');" type="submit" class="btn btn-danger btn-sm"><i class="fas fa-times"></i></button>
                                            </form>
                                        </div>
                                    </div>

                                </li>
                            @endforeach
                        </ul>
                        @else
                        <p class="m-3">
                            Nincs aktív egyéb szolgáltatás.
                        </p>
                        @endif
                    </div>
                    <div class="card-footer text-muted">
                        * A szolgáltatások visszavonhatóak. Ha a teljes időtartam fele előtt vonják vissza őket akkor, 50%-os visszatérítés jár.
                        Ezt személyesen tudjuk visszaszolgáltatni a bölcsödében.
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="card" style="">
                    <div class="card-header">
                      Lejárt egyéb szolgáltatás
                    </div>
                    <div style="max-height: 300px;overflow-y: scroll">
                        @if (count($inactiveSpecialSubscriptions) > 0)
                        <ul class="list-group list-group-flush">
                            @foreach ($inactiveSpecialSubscriptions as $sub)
                                <li class="list-group-item">{{ $sub->product }} | <span class="text-danger">lejárt ekkor: {{ $sub->valid_until }}</span></li>
                            @endforeach
                        </ul>
                        @else
                            <p class="m-3">Nincs lejárt egyéb szolgáltatás.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>

        document.addEventListener("DOMContentLoaded", function(){
            const element = document.getElementById("message-container");
            element.scrollTop = element.scrollHeight;
        });

    </script>
@endsection
