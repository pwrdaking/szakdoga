@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-8 mt-2">
        <h2>Nevelő lista</h2>

        <a href="{{ route('teacher.register') }}" class="btn btn-warning float-right mb-1">Nevelő hozzáadása</a>

        @if(count($teachers) > 0)
            <table class="table table-bordered">
                <tr>
                    <td>Név</td>
                    <td>Telefon</td>
                    <td>Email</td>
                    <td>Műveletek</td>
                </tr>
                @foreach($teachers as $teacher)
                    <tr>
                        <td>{{ $teacher->parent_name }}</td>
                        <td>{{ $teacher->phone }}</td>
                        <td>{{ $teacher->email }}</td>
                        <td>
                            <form action="{{ route('teacher.destroy', $teacher->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button onclick="return confirm('Biztos, hogy törölni szeretnéd a {{ $teacher->parent_name }} nevű felhasználót?');" type="submit" class="btn btn-danger">Törlés</button>
                            </form>

                        </td>
                    </tr>
                @endforeach
            </table>

            @if(count($teachers) > 0)
                <div class="pagination">
                    <?php echo $teachers->render();  ?>
                </div>
            @endif

        @else
            <i>Nincs még tanár felvéve.</i>
        @endif
    </div>
</div>

@endsection
