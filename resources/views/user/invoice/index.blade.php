@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-8 mt-2">
        <h2>Számlák</h2>

        @if(count($orders) > 0)
            <table class="table table-bordered">
                <tr>
                    <td>Szolgáltatások</td>
                    <td>Ár</td>
                    <td>Állapot</td>
                </tr>
                @foreach($orders as $order)
                    <tr>
                        <td>{{ $order->items }}</td>
                        <td>{{ $order->price }} Ft</td>
                        <td>
                            <span class="text-success">Fizetve</span>
                        </td>
                    </tr>
                @endforeach
            </table>

        @else
            <i>Nincs még számlád.</i>
        @endif

    </div>
</div>
@endsection
