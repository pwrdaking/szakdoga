@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row w-100">
        <div class="col-12 d-flex justify-content-center align-items-center cart-height w-100">
            <div class="row overflow-hidden">
                <div class="col-12 d-flex align-items-center justify-content-center mt-5">
                    <h2 class="empty-cart-title text-center">
                        Sikeres fizetés!
                    </h2>

                </div>
                <div class="col-12 d-flex justify-content-center align-items-center" >
                    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                    <lottie-player src="https://assets5.lottiefiles.com/packages/lf20_8EfvqQ.json"  background="transparent"  speed="1"  style="width: 400px; height: 400px;"  loop  autoplay></lottie-player>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
