@extends('layouts.app')

@section('content')
<div class="container pt-5">

    <div class="row justify-content-center mb-3">
        @if(request()->get('payment'))
            <div class="alert alert-danger alert-dismissable">
                <div class="row justify-content-end mr-1"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>
                <div class="row px-5">
                    <h4>Fizetés megszakítva</h4>
                </div>
            </div>
        @endif
    </div>

    @if(count($items) < 1)
        <div class="row w-100">
            <div class="col-12 d-flex justify-content-center align-items-center cart-height w-100">
                <div class="row overflow-hidden">
                    <div class="col-12 d-flex align-items-center justify-content-center slide-top">
                        <h2 class="empty-cart-title text-center">
                            Jelenleg a kosarad üres!
                            <br>
                            <a href="/product">Itt </a>
                            találod az elérhető szolgáltatásaink.
                        </h2>

                    </div>
                    <div class="col-12 d-flex justify-content-center align-items-center" >
                        <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                        <lottie-player src="https://assets6.lottiefiles.com/packages/lf20_os6kzfdd.json"  background="transparent"  speed="1"  style="width: 600px; height: 600px;"  loop  autoplay></lottie-player>
                    </div>
                </div>
            </div>
        </div>
    @else

    <div class="row py-5 justify-content-around">
        <div class="col-12 col-lg-8">
            <div class="card bg-light mb-3">
                <div class="row justify-content-between mx-0 card-header">
                    <h3>Kosarad</h3>
                    <form action="{{ route('cart.destroy.all') }}"
                        method="POST"
                        onsubmit="deleteBtn.disabled = true;">
                        {{ csrf_field() }}
                        {{method_field('DELETE')}}
                        <button class="btn btn-danger" type="submit" name="deleteBtn">
                            Kosár ürítése
                        </button>
                    </form>
                </div>
                <div class="card-body">
                    @foreach($items as $key => $item)
                        <div class="d-flex justify-content-between">
                            <h2 class="mb-0">
                                {{ $item->name }}
                                <span class="text-opacity-50">
                                    ({{$item->options->childName}})
                                </span>
                            </h2>
                            <form action="{{ route('cart.destroy', ['id' => $item->rowId]) }}"
                                method="POST"
                                onsubmit="deleteBtn.disabled = true;">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button class="btn" type="submit" name="deleteBtn"><i class="fas fa-trash trash text-danger"></i>
                                </button>
                            </form>
                        </div>

                        <div class="d-flex pl-2 mb-2">
                            <span class="text-opacity-50">
                                {{$item->options->description}}
                            </span>
                        </div>

                        <div class="d-flex pl-2 justify-content-between">
                            <p class="mb-0">{{$item->qty}} db</p>
                            <p class="mb-0">{{$item->price}} Ft(+ 27% áfa)</p>
                        </div>
                        <hr>
                    @endforeach
                    <div class="row justify-content-between mx-0">
                        <div>
                            <span>Áfa nélkül:</span>
                            <br>
                            <p class="mb-2">{{$cartSubTotal}} Ft </p>
                        </div>

                        <div>
                            <span>
                                Fietendő(tartalmazza az Áfá-t):
                            </span>
                             <br>
                            <h5> <u>{{$cartTotal}} Ft</u>  </h5>
                        </div>
                    </div>
                    <div class="row mt-2 mx-0 justify-content-end">
                        <form action="/checkout" method="POST" onsubmit="paymentStarted()">
                            @csrf
                            <button onclick="" id="paymentButton" class="btn btn-success" type="submit">Tovább a pénztárhoz</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-4">
            <div class="card bg-light mb-3">
                <div class="row justify-content-between mx-0 card-header">
                    <h6>Szakdolgozat megjegyzések</h6>
                </div>
                <div class="card-body">
                    A tovább pénztár gombra kattintva átirányit a Stripe-ra, mivel egy csak teszt
                    módban van ezért picit lassú.
                    A fizetés ezzel a kártyaszámmal tesztelhető: "4242424242424242", a többi adat mindegy hogy mi.
                </div>
            </div>
        </div>

    </div>
    @endif
</div>

<script>
    function paymentStarted() {
        document.getElementById("paymentButton").disabled = true;
    }
</script>
@endsection
