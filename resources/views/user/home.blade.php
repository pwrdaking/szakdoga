@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center pt-5">
            <div class="col-6">
                <div class="card" style="">
                    <div class="card-body">
                      <h4 class="card-title">Üdv a Csodabölcsi admin felületén!</h4>
                      <h6 class="card-subtitle mb-2 text-muted">Bejelentkeve: {{Auth::user()->parent_name}}, {{Auth::user()->type == 0 ? "Nevelő" : "Szülő" }}</h6>
                      <p class="card-text">Ha bármiben segítségére lehetünk keressen minket bizalommal.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
