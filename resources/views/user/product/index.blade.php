@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-8 mt-3">

        <div class="row justify-content-center mb-3">
            @if(session('success'))
                <div class="alert alert-success alert-dismissable">
                    <div class="row justify-content-end mr-1">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    </div>
                    <div class="row px-5">
                        <h4>{{session('success')}}</h4>
                    </div>
                </div>
            @endif
        </div>

        <div class="row mb-3">
            <h2>Alap szolgáltatások</h2>
        </div>

        @if(count($baseProducts) > 0)
            <div class="row justify-content-around">
                @foreach($baseProducts as $product)
                    <div class="card col-5 col-lg-3 mx-3 mb-3">
                        <div class="card-body">
                            <h5 class="card-title" style="min-height: 45px">{{ $product->title }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $product->validity_count }} hetes</h6>
                            <p class="card-text mb-1" style="min-height: 80px">{{ $product->description }}</p>

                            <hr>
                            <form action="{{ route('cart.add') }}" onsubmit="sendBtn.disabled = true;" method="POST">
                                <div class="row mb-2">
                                    <label for="child" class=" col-form-label text-md-right">Kinek?</label>

                                    <select class="custom-select form-control" id="child" name="child">
                                        @foreach ($children as $child)
                                            <option value="{{$child->id}}">{{$child->child_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="row justify-content-around">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$product->id}}">
                                    <button class="btn btn-warning" name="specialSendBtn"><i class="fas fa-shopping-cart"></i> Kosárba</button>
                                </div>
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <i>Nincs még megvásárolható szolgáltatás.</i>
        @endif

        <hr>

        <div class="row mb-3">
            <h2>Egyéb szolgáltatások</h2>
        </div>

        @if(count($specialProducts) > 0)
            <div class="row justify-content-around">
                @foreach($specialProducts as $product)
                    <div class="card col-5 col-lg-3 mx-3 mb-3">
                        <div class="card-body">
                            <h5 class="card-title" style="min-height: 45px">{{ $product->title }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $product->validity_count }} hetes</h6>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $product->price }} Ft</h6>
                            <p class="card-text mb-1" style="min-height: 80px">{{ $product->description }}</p>

                            <hr>

                            <form action="{{ route('cart.add') }}" onsubmit="sendBtn.disabled = true;" method="POST">
                                <div class="row mb-2">
                                    <label for="child" class=" col-form-label text-md-right">Kinek?</label>

                                    <select class="custom-select form-control" id="child" name="child">
                                        @foreach ($children as $child)
                                            <option value="{{$child->id}}">{{$child->child_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="row justify-content-around">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$product->id}}">
                                    <button class="btn btn-warning" name="specialSendBtn"><i class="fas fa-shopping-cart"></i> Kosárba</button>
                                </div>
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <i>Nincs még megvásárolható egyéb szolgáltatás.</i>
        @endif

    </div>
</div>

@endsection
