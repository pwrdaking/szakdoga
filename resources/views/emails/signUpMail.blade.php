<!DOCTYPE html>

<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
    <head>
        <title></title>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <!--[if mso]>
            <xml>
                <o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG /></o:OfficeDocumentSettings>
            </xml>
        <![endif]-->
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Bitter" rel="stylesheet" type="text/css" />
        <!--<![endif]-->
        <style>
            * {
                box-sizing: border-box;
            }

            body {
                margin: 0;
                padding: 0;
            }

            /*th.column{
	padding:0
}*/

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: inherit !important;
            }

            #MessageViewBody a {
                color: inherit;
                text-decoration: none;
            }

            p {
                line-height: inherit;
            }

            @media (max-width: 660px) {
                .icons-inner {
                    text-align: center;
                }

                .icons-inner td {
                    margin: 0 auto;
                }

                .row-content {
                    width: 100% !important;
                }

                .image_block img.big {
                    width: auto !important;
                }

                .stack .column {
                    width: 100%;
                    display: block;
                }
            }
        </style>
    </head>
    <body style="background-color: #ffffff; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;">
        <table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff;" width="100%">
            <tbody>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000;" width="640">
                                            <tbody>
                                                <tr>
                                                    <td
                                                        class="column"
                                                        style="
                                                            mso-table-lspace: 0pt;
                                                            mso-table-rspace: 0pt;
                                                            font-weight: 400;
                                                            text-align: left;
                                                            vertical-align: top;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                            border-top: 0px;
                                                            border-right: 0px;
                                                            border-bottom: 0px;
                                                            border-left: 0px;
                                                        "
                                                        width="100%"
                                                    >
                                                        <table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                                            <tr>
                                                                <td style="width: 100%; padding-right: 0px; padding-left: 0px;">
                                                                    <div align="center" style="line-height: 10px;">
                                                                        <img
                                                                            alt="I'm an image"
                                                                            class="big"
                                                                            src="{{ env('APP_URL')}}/images/bitmap.png"
                                                                            style="display: block; height: auto; border: 0; width: 640px; max-width: 100%;"
                                                                            title="I'm an image"
                                                                            width="640"
                                                                        />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                                            <tr>
                                                                <td style="padding-bottom: 10px; padding-left: 20px; padding-right: 20px; padding-top: 35px;">
                                                                    <div style="font-family: sans-serif;">
                                                                        <div style="font-size: 16px; font-family: Bitter, Georgia, Times, Times New Roman, serif; mso-line-height-alt: 19.2px; color: #555555; line-height: 1.2;">
                                                                            <p style="margin: 0; font-size: 16px; text-align: center;">
                                                                                <span style="font-size: 46px; color: #d8a47f;"><strong>Sikeres felvétel!</strong></span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                                            <tr>
                                                                <td style="padding-bottom: 20px; padding-left: 10px; padding-right: 10px; padding-top: 10px;">
                                                                    <div style="font-family: Arial, sans-serif;">
                                                                        <div style="font-size: 14px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 21px; color: #4e5264; line-height: 1.5;">
                                                                            <p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 24px;">
                                                                                <span style="font-size: 16px; color: #4e5264;">
                                                                                    Kedves {{ $emailData['parent_name'] }}, örömmel értesítem, hogy {{ $emailData['child_name'] }} felvételt nyert bölcsödénkbe.
                                                                                    A "{{ $emailData['group_name'] }}" csoportba került!
                                                                                    Mostmár csak annyi teendője van, hogy kiválasztja az önöknek megfelelő bölcsöde tervet és befizeti az számlát.
                                                                                    <br>
                                                                                    Köszönjük, hogy a mi bölcsödénket választotta!
                                                                                    <br>
                                                                                    <br>
                                                                                    Gáti Gergő, Bölcsöde igazgató
                                                                                </span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000;" width="640">
                                            <tbody>
                                                <tr>
                                                    <td
                                                        class="column"
                                                        style="
                                                            mso-table-lspace: 0pt;
                                                            mso-table-rspace: 0pt;
                                                            font-weight: 400;
                                                            text-align: left;
                                                            vertical-align: top;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                            border-top: 0px;
                                                            border-right: 0px;
                                                            border-bottom: 0px;
                                                            border-left: 0px;
                                                        "
                                                        width="100%"
                                                    >
                                                        <table border="0" cellpadding="0" cellspacing="0" class="icons_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                                            <tr>
                                                                <td style="color: #9d9d9d; font-family: inherit; font-size: 15px; padding-bottom: 5px; padding-top: 5px; text-align: center;">
                                                                    <table cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                                                        <tr>
                                                                            <td style="text-align: center;">
                                                                                <!--[if vml]><table align="left" cellpadding="0" cellspacing="0" role="presentation" style="display:inline-block;padding-left:0px;padding-right:0px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><![endif]-->
                                                                                <!--[if !vml]><!-->
                                                                                <table
                                                                                    cellpadding="0"
                                                                                    cellspacing="0"
                                                                                    class="icons-inner"
                                                                                    role="presentation"
                                                                                    style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; display: inline-block; margin-right: -4px; padding-left: 0px; padding-right: 0px;"
                                                                                >
                                                                                    <!--<![endif]-->
                                                                                    <tr>
                                                                                        <td style="text-align: center; padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 6px;">
                                                                                            <a href="https://www.designedwithbee.com/">
                                                                                                <img
                                                                                                    align="center"
                                                                                                    alt="Designed with BEE"
                                                                                                    class="icon"
                                                                                                    height="32"
                                                                                                    src="{{ env('APP_URL')}}/images/bee.png"
                                                                                                    style="display: block; height: auto; border: 0;"
                                                                                                    width="34"
                                                                                                />
                                                                                            </a>
                                                                                        </td>
                                                                                        <td
                                                                                            style="
                                                                                                font-family: Bitter, Georgia, Times, Times New Roman, serif;
                                                                                                font-size: 15px;
                                                                                                color: #9d9d9d;
                                                                                                vertical-align: middle;
                                                                                                letter-spacing: undefined;
                                                                                                text-align: center;
                                                                                            "
                                                                                        >
                                                                                            <a href="https://www.designedwithbee.com/" style="color: #9d9d9d; text-decoration: none;">Designed with BEE</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- End -->
    </body>
</html>
