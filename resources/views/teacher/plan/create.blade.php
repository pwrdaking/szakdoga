@extends('layouts.app')

@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">Terv létrehozása</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('plan.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="week" class="col-md-2 col-form-label text-md-right">Hét sorszáma</label>

                            <div class="col-md-6">
                                <input id="week" type="number" class="form-control @error('week') is-invalid @enderror" name="week" value="{{ old('week') }}" autocomplete="title" autofocus required>

                                @error('week')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="body" class="col-md-2 col-form-label text-md-right">Terv</label>

                            <div class="col-md-6">
                                <textarea id="body" type="text" class="form-control @error('body') is-invalid @enderror" name="body" value="{{ old('body') }}" autocomplete="title" autofocus required></textarea>

                                @error('body')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <input type="hidden" name="group_id" value="{{ request()->get('group_id') }}">

                        <div class="form-group row justify-content-center text-center mt-3 mb-0">
                            <button type="submit" class="btn btn-primary" id="btn-save">Mentés</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
