@extends('layouts.app')

@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">Csoport létrehozása</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('group.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">Név</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="name" value="{{ old('title') }}" autocomplete="title" placeholder="Cím" autofocus required>

                                @error('parent_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="teacher-name" class="col-md-2 col-form-label text-md-right">Nevelő</label>

                            <div class="col-md-6">
                                <select class="custom-select form-control" id="teacher-name" name="teacher">
                                    @foreach ($teachers as $teacher)
                                        <option value="{{$teacher->id}}">{{$teacher->parent_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row justify-content-center text-center mt-3 mb-0">
                            <button type="submit" class="btn btn-primary" id="btn-save">Mentés</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
