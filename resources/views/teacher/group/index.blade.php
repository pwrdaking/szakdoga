@extends('layouts.app')

@section('content')

@if(count($unConfirmedChildren) > 0)
<div class="row justify-content-center mt-2">
    <div class="col-12 col-md-8 mt-2">
        <h2>Beíratásra váró gyerekek</h2>

            <table class="table table-bordered">
                <tr>
                    <td>Név</td>
                    <td>Érzékenység</td>
                    <td>Alapbetegség</td>
                    <td>Születésnap</td>
                    <td>Csoporthoz adás</td>
                </tr>
                @foreach($unConfirmedChildren as $child)
                    <tr>
                        <td><a href="/child/{{ $child->id }}?from=group">{{ $child->child_name }}</a></td>
                        <td>{{ $child->sensitivity }}</td>
                        <td>{{ $child->base_disease }}</td>
                        <td>{{ $child->birthday }}</td>
                        <td class="">
                            <form method="POST" action="{{ route('child.confirm', $child->id) }}">
                                @csrf
                                <select class="custom-select " id="teacher-name" name="group">
                                    @foreach ($groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary float-right mt-1 mr-1 mb-1">Hozzáad</button>
                            </form>
                        </td>
                        <td class=" m-0 justify-content-center">
                            <form action="{{ route('child.destroy', $child->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button onclick="return confirm('Biztos, hogy törölni szeretnéd {{ $child->child_name }}-t?');" type="submit" class="btn btn-danger mr-1">Törlés</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
    </div>
</div>
@endif

<div class="row justify-content-center mt-2">
    <div class="col-12 col-md-8 mt-2">
        <div class="row justify-content-between">
            <h2>Csoportok</h2>
            @if (!$isAlreadyHaveGroup)
                <a href="{{ route('group.create') }}" class="btn btn-warning float-right mb-1">Új csoport</a>
            @endif
        </div>

        <div class="row mt-4">
            @foreach($groups as $group)
            <div class="col-12 col-md-6 px-5">
                <div class="card" style="">
                    <div class="card-body">
                        <div class="row justify-content-between mx-0">
                            <h4 class="card-title">{{ $group->name }}</h4>
                            <div>
                                @if($group->teacher_id == Auth::user()->id)
                                    <i class="fas fa-crown" style="color: #f0dc82" title="A te csoportod"></i>
                                @endif
                                <a href="/group/{{ $group->id }}/edit"><i class="fas fa-edit"></i></a>
                            </div>
                        </div>
                        <h6 class="card-subtitle mb-2 text-muted">Nevelő: {{ $group->teacher }}</h6>
                        <hr>
                        <p class="mb-0">Gyerekek:</p>
                        @if(count($group->children) > 0)
                            <ul class="list-group list-group-flush">
                                @foreach($group->children as $child)
                                    <li class="list-group-item">
                                        <div class="row justify-content-between align-items-center">
                                            <a href="/child/{{ $child->id }}?from=group">{{ $child->child_name }}</a>
                                            <form action="{{ route('child.remove') }}" method="POST">
                                                {{ csrf_field() }}
                                                {{method_field('DELETE')}}
                                                <input type="hidden" id="" name="child" value="{{ $child->id }}">
                                                <button onclick="return confirm('Biztos, hogy el szeretnéd távolítani {{ $child->child_name }}-t a csoportból?');" type="submit" class="btn btn-danger  btn-sm mr-1"><i class="fas fa-times"></i></button>
                                            </form>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <p class="ml-3 mt-2 text-opacity-50">
                                Nincs még gyerek a csoportban.
                            </p>
                        @endif

                        <hr class="mt-0">
                        <form action="{{ route('group.destroy', $group->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{method_field('DELETE')}}
                            <button onclick="return confirm('Biztos, hogy törölni szeretnéd a {{ $group->name }} csoportot?');" type="submit" class="btn btn-danger mr-1">Csoport törlés</button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
