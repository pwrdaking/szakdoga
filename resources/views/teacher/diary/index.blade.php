@extends('layouts.app')

@section('content')
    <div class="container pb-4">

        @if (isset($group->name))
            <h2 class="font-bold border-bottom border-3 border-dark my-4 pb-2">
                {{ $group->name }}
            </h2>
            <div class="row">

                <div class="col-6">
                    <div class="card">
                        <div class="d-flex card-header justify-content-between">
                            <h4 class="mb-0">Hiányzások</h4>
                        </div>
                        <div class="card-body" style="max-height: 350px;overflow-y: scroll;">
                            @if (count($absences) > 0)
                                @foreach ($absences as $absence)
                                <div class="row">
                                        <div class="col-10">
                                            <p class="mb-0">{{ $absence->child_name }} ({{ $absence->start }}) - ({{ $absence->end }})</p>
                                            <p class="mb-0">Ok: <br> {{ $absence->cause }}</p>
                                        </div>
                                        <div class="d-flex col-2 align-items-center">
                                            <div class="w-100">
                                                <form action="{{ route('absence.destroy', $absence->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{method_field('DELETE')}}
                                                    <button onclick="return confirm('Biztos, hogy törölni szeretnéd a hiányzást?');" type="submit" class="btn btn-danger mr-1"><i class="fas fa-trash"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-1">
                                @endforeach
                            @else
                                <h5>Nincsenek hiányzások.</h5>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <h4> Gyerekek:</h4>
                    <div class="ml-3">
                        <div class="card">
                            <ul class="list-group list-group-flush">
                                @foreach($children as $child)
                                    <li class="list-group-item">
                                        <a href="/child/{{$child->id}}?from=diary" class="text-black" style="text-decoration: none;">
                                            <div class="d-flex justify-content-between">
                                                <span>{{ $child->child_name }}</span>
                                                <i class="fas fa-chevron-right" style="line-height: inherit;"></i>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                          </div>
                    </div>
                </div>
            </div>
        @else
            <div class="mt-5">
                <h3>
                    Nincs még csoportod
                    <a href="/group">itt</a>
                    tudsz készíteni csoportot!
                </h3>
            </div>
        @endif


        <div class="row border-bottom border-1 border-dark my-4 pb-2 justify-content-between">
            <h5 class="font-bold mt-3 mb-0">
                Heti tervek:
            </h5>
            <a href="/plan/create?group_id={{ $group->id }}" class="btn btn-primary btn-sm float-right mt-1 mr-1 mb-1">Hozzáad</a>
        </div>

        <div class="d-flex mx-0 my-4 pb-3" style="overflow-x: scroll; min-height: 500px;">

            @if (count($plans) > 0)
                @foreach($plans as $plan)
                    <div class="card mx-2" style="min-width: 400px;">
                        <div class="d-flex card-header justify-content-between">
                            <h4 class="mb-0">{{ $plan->week }}. heti terv</h4>
                            <div class="row">
                                <a href="/plan/{{$plan->id}}/edit" class="mr-3"><i class="fas fa-edit"></i></a>
                                <form action="{{ route('plan.destroy', $plan->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{method_field('DELETE')}}
                                    <button onclick="return confirm('Biztos, hogy törölni szeretnéd a {{ $plan->week }}. heti tervet?');" type="submit" class="btn btn-danger btn-sm mr-1"><i class="fas fa-trash"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            {{ $plan->body }}
                        </div>
                    </div>
                @endforeach
            @else
                <h5>Nincs még terv felvéve.</h5>
            @endif

        </div>

    </div>
@endsection
