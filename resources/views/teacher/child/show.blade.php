@extends('layouts.app')

@section('content')
    <div class="container pb-4 mt-3">

        <div class="row justify-content-between mb-3">
            <a href="/{{ app('request')->input('from') }}" class="btn btn-primary">Vissza</a>
            @if(isset($child->base_subscription_until))
            <span class="text-success">
                Bölcsödei alapdíj befizetve eddig: {{ $child->base_subscription_until }}.
            </span>
            @else
            <span class="text-danger">
                Bölcsödei alapdíjja nincs befizetve!
            </span>
            @endif
        </div>

        <div class="row justify-content-between mb-4">

            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-0">{{ $child->child_name }}</h4>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title">{{ $child->group_name }}</h5>

                      <p class="card-text mb-0">Érzékenység:</p>
                      <p class="ml-3">{{ $child->sensitivity }}</p>

                      <p class="card-text mb-0">Alapbetegség:</p>
                      <p class="ml-3">{{ $child->base_disease }}</p>

                      <p class="card-text mb-0">Kor:</p>
                      <p class="ml-3">{{ $child->months }} hónapos</p>

                      <p class="card-text mb-0">Szülő adatai:</p>
                      <p class="ml-3 mb-0">{{ $child->parent->parent_name }}</p>
                      <p class="ml-3 mb-0">{{ $child->parent->phone }}</p>
                      <p class="ml-3 mb-0">{{ $child->parent->email }}</p>
                    </div>
                  </div>
            </div>

            <div class="col-6">
                <div class="card">
                    <div class="d-flex card-header justify-content-between">
                        <h4 class="mb-0">Hiányzások</h4>
                    </div>
                    <div class="card-body" style="max-height: 350px;overflow-y: scroll;">
                        @if (count($child->absences) > 0)
                            @foreach ($child->absences as $absence)
                            <div class="row">
                                    <div class="col-10">
                                        <p class="mb-0">({{ $absence->start }}) - ({{ $absence->end }})</p>
                                        <p class="mb-0">Ok: <br> {{ $absence->cause }}</p>
                                    </div>
                                    <div class="d-flex col-2 align-items-center">
                                        <div class="w-100">
                                            <form action="{{ route('absence.destroy', $absence->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{method_field('DELETE')}}
                                                <button onclick="return confirm('Biztos, hogy törölni szeretnéd a hiányzást?');" type="submit" class="btn btn-danger mr-1"><i class="fas fa-trash"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-1">
                            @endforeach
                        @else
                            <h5>Nincs még hiányzása.</h5>
                        @endif
                    </div>
                </div>
            </div>

        </div>

        <div class="row justify-content-center pb-5">
            <div class="col-8">
                <div class="card">
                    <div class="d-flex card-header justify-content-between">
                        <h4 class="mb-0">Megjegyzések</h4>
                    </div>
                    <div id="message-container" class="card-body" style="min-height: 400px; max-height: 600px;overflow-y: scroll;">

                        @if(count($child->messages))
                            @foreach ($child->messages as $message)
                            <div class="card bg-primary mb-3 text-white">
                                <div class="card-body pt-3 pb-1">
                                    <div class="row mx-0">
                                        <div class="col-11">{{ $message->message }}</div>
                                        <div class="row col-1 mx-0 px-0 justify-content-end">
                                            <form action="{{ route('message.destroy', $message->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{method_field('DELETE')}}
                                                <button onclick="return confirm('Biztos, hogy törölni szeretnéd a megjegyzést?');" type="submit" class="btn btn-danger mr-1"><i class="fas fa-trash"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row justify-content-end">
                                        <span class="text-message-date">
                                            {{ $message->created_at }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <h5>
                                Még nincs felvéve megjegyzés.
                            </h5>
                        @endif

                    </div>
                    <div class="card-footer">

                        <form action="{{ route('message.store') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group mb-2">
                                <textarea class="form-control" id="message" name="message" rows="3"></textarea>
                            </div>
                            <input type="hidden" name="child_id" value="{{ $child->id }}">
                            <div class="row justify-content-center">
                                <button type="submit" class="btn btn-primary" style="min-width: 150px;">Küldés</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <div class="card" style="">
                    <div class="card-header">
                      Aktív egyéb szolgáltatások
                    </div>
                    <div style="max-height: 250px;overflow-y: scroll">
                        @if (count($activeSpecialSubscriptions) > 0)
                        <ul class="list-group list-group-flush">
                            @foreach ($activeSpecialSubscriptions as $sub)
                                <li class="list-group-item">{{ $sub->product }} | <span class="text-success">eddig: {{ $sub->valid_until }}</span></li>
                            @endforeach
                        </ul>
                        @else
                        <p class="m-3">
                            Nincs aktív egyéb szolgáltatás.
                        </p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="card" style="">
                    <div class="card-header">
                      Lejárt egyáb szolgáltatások
                    </div>
                    <div style="max-height: 300px;overflow-y: scroll">
                        @if (count($inactiveSpecialSubscriptions) > 0)
                        <ul class="list-group list-group-flush">
                            @foreach ($inactiveSpecialSubscriptions as $sub)
                                <li class="list-group-item">{{ $sub->product }} | <span class="text-danger">lejárt ekkor: {{ $sub->valid_until }}</span></li>
                            @endforeach
                        </ul>
                        @else
                            <p class="m-3">Nincs lejárt egyéb szolgáltatás.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>


    <script>

        document.addEventListener("DOMContentLoaded", function(){
            const element = document.getElementById("message-container");
            element.scrollTop = element.scrollHeight;
        });

    </script>

@endsection
