@extends('layouts.app')

@section('content')
<div class="container justify-content-center">
    <div class="col-12 mt-3">
        <div class="row justify-content-between mb-3">
            <h2>Alap Szolgáltatások</h2>
            <a href="{{ route('product.create') }}" class="btn btn-warning float-right mb-1">Új szolgáltatás</a>
        </div>

        @if(count($baseProducts) > 0)
            <div class="row justify-content-around">
                @foreach($baseProducts as $product)
                    <div class="card col-5 col-lg-3 mx-3 mb-3">
                        <div class="card-body">
                            <h5 class="card-title" style="min-height: 45px">{{ $product->title }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $product->validity_count }} hetes</h6>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $product->price }} Ft</h6>
                            <p class="card-text mb-1" style="min-height: 80px">{{ $product->description }}</p>

                            <hr>

                            <div class="row justify-content-around">
                                <a href="{{ route('product.edit', $product->id) }}" class="btn btn-primary float-right mr-1 mb-1">Szerkesztés</a>
                                <form action="{{ route('product.destroy', $product->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{method_field('DELETE')}}
                                    <button onclick="return confirm('Biztos, hogy törölni szeretnéd a {{ $product->title }} nevű szolgáltatást?');" type="submit" class="btn btn-danger mr-1">Törlés</button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <i>Nincs még alap szolgáltatás felvéve</i>
        @endif
    </div>

    <hr>

    <div class="col-12 mt-3">
        <div class="row justify-content-between mb-3">
            <h2>Egyéb Szolgáltatások</h2>
        </div>

        @if(count($specialProducts) > 0)
            <div class="row justify-content-around">
                @foreach($specialProducts as $product)
                    <div class="card col-5 col-lg-3 mx-3 mb-3">
                        <div class="card-body">
                            <h5 class="card-title" style="min-height: 45px">{{ $product->title }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $product->validity_count }} hetes</h6>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $product->price }} Ft</h6>
                            <p class="card-text mb-1" style="min-height: 80px">{{ $product->description }}</p>

                            <hr>

                            <div class="row justify-content-around">
                                <a href="{{ route('product.edit', $product->id) }}" class="btn btn-primary float-right mr-1 mb-1">Szerkesztés</a>
                                <form action="{{ route('product.destroy', $product->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{method_field('DELETE')}}
                                    <button onclick="return confirm('Biztos, hogy törölni szeretnéd a {{ $product->title }} nevű szolgáltatást?');" type="submit" class="btn btn-danger mr-1">Törlés</button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <i>Nincs még alap szolgáltatás felvéve</i>
        @endif
    </div>

</div>

@endsection
