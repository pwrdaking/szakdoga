@extends('layouts.app')

@section('styles')
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
@endsection

@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">Menu létrehozása</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('meal.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="week" class="col-md-2 col-form-label text-md-right">Hét sorszáma</label>

                            <div class="col-md-6">
                                <input id="week" type="number" class="form-control @error('week') is-invalid @enderror" name="week" value="{{ old('week') }}" autocomplete="title" autofocus required>

                                @error('week')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>

                        @foreach ($daysEn as $day)
                        <h5>{{ $daysHu[$loop->index] }}</h5>
                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">Reggeli</label>

                            <div class="col-md-6">
                                <input id="{{ $day}}-breakfast" type="text" class="form-control @error($day. 'breakfast') is-invalid @enderror"
                                name="{{ $day }}-breakfast" value="{{ old($day. '-breakfast') }}"
                                autofocus required>

                                @error('{{ $day}}-breakfast')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">Ebéd</label>

                            <div class="col-md-6">
                                <input id="{{ $day}}-lunch" type="text" class="form-control @error($day. '-lunch') is-invalid @enderror"
                                name="{{ $day }}-lunch" value="{{ old($day. '-lunch') }}"
                                autofocus required>

                                @error('{{ $day}}-lunch')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">Uzsonna</label>

                            <div class="col-md-6">
                                <input id="{{ $day}}-dinner" type="text" class="form-control @error($day. '-dinner') is-invalid @enderror"
                                name="{{ $day }}-dinner" value="{{ old($day. '-dinner') }}"
                                autofocus required>

                                @error('{{ $day}}-dinner')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        @endforeach

                        <div class="form-group row justify-content-center text-center mt-3 mb-0">
                            <button type="submit" class="btn btn-primary" id="btn-save">Mentés</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
