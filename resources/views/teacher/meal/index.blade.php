@extends('layouts.app')

@section('content')
    <div class="container pb-4">

        <div class="row border-bottom border-1 border-dark my-4 pb-2 justify-content-between">
            <h5 class="font-bold mt-3 mb-0">
                Heti menük:
            </h5>
            @if(Auth::user()->type == 0)
            <a href="/meal/create" class="btn btn-primary btn-sm float-right mt-1 mr-1 mb-1">Hozzáad</a>
            @endif
        </div>

        <div class="d-flex mx-0 my-4 pb-3" style="overflow-x: scroll; min-height: 500px;">

            @if (count($meals) > 0)
                @foreach($meals as $meal)
                    <div class="card mx-2" style="min-width: 400px;">
                        <div class="d-flex card-header justify-content-between">
                            <h4 class="mb-0">{{ $meal->week }}. heti menu</h4>
                            @if(Auth::user()->type == 0)
                            <div class="row">
                                <form action="{{ route('meal.destroy', $meal->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{method_field('DELETE')}}
                                    <button onclick="return confirm('Biztos, hogy törölni szeretnéd a {{ $meal->week }}. heti menut?');" type="submit" class="btn btn-danger btn-sm mr-1"><i class="fas fa-trash"></i></button>
                                </form>
                            </div>
                            @endif
                        </div>
                        <div class="card-body">
                            <h5>Hétfő:</h5>
                            <p class="ml-3 mb-0">Reggeli: {{ $meal->monday_breakfast }}</p>
                            <p class="ml-3 mb-0">Ebéd: {{ $meal->monday_lunch }}</p>
                            <p class="ml-3 mb-0">Uzsonna: {{ $meal->monday_dinner }}</p>
                            <hr>
                            <h5>Kedd:</h5>
                            <p class="ml-3 mb-0">Reggeli: {{ $meal->tuesday_breakfast }}</p>
                            <p class="ml-3 mb-0">Ebéd: {{ $meal->tuesday_lunch }}</p>
                            <p class="ml-3 mb-0">Uzsonna: {{ $meal->tuesday_dinner }}</p>
                            <hr>
                            <h5>Szerda:</h5>
                            <p class="ml-3 mb-0">Reggeli: {{ $meal->wednessday_breakfast }}</p>
                            <p class="ml-3 mb-0">Ebéd: {{ $meal->wednessday_lunch }}</p>
                            <p class="ml-3 mb-0">Uzsonna: {{ $meal->wednessday_dinner }}</p>
                            <hr>
                            <h5>Csütörtök:</h5>
                            <p class="ml-3 mb-0">Reggeli: {{ $meal->thursday_breakfast }}</p>
                            <p class="ml-3 mb-0">Ebéd: {{ $meal->thursday_lunch }}</p>
                            <p class="ml-3 mb-0">Uzsonna: {{ $meal->thursday_dinner }}</p>
                            <hr>
                            <h5>Péntek:</h5>
                            <p class="ml-3 mb-0">Reggeli: {{ $meal->friday_breakfast }}</p>
                            <p class="ml-3 mb-0">Ebéd: {{ $meal->friday_lunch }}</p>
                            <p class="ml-3 mb-0">Uzsonna: {{ $meal->friday_dinner }}</p>
                            <hr>
                        </div>
                    </div>
                @endforeach
            @else
                <h5>Nincs még menu felvéve.</h5>
            @endif

        </div>

    </div>
@endsection
