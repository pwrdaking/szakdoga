@extends('layouts.app')

@section('styles')
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
@endsection

@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">Hír szerkesztése</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('news.update', $news->id) }}">
                        @csrf
                        {{ method_field("PUT") }}
                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">Cím*</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $news->title }}" autocomplete="title" placeholder="Cím" autofocus required>

                                @error('parent_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="">
                            <label class="mb-0 col-form-label">Szöveg törzs:</label>
                            <textarea id="summernote" name="description">
                                {!! $news->description !!}
                            </textarea>
                        </div>

                        <div class="form-group row justify-content-center text-center mt-3 mb-0">
                            <button type="submit" class="btn btn-primary" id="btn-save">Mentés</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#summernote').summernote({
        placeholder: '',
        tabsize: 2,
        height: 120,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
        ]
      });
</script>

@endsection
