@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-8 mt-2">
        <h2>Hírek</h2>

        <a href="{{ route('news.create') }}" class="btn btn-warning float-right mb-1">Új hír</a>

        @if(count($newsList) > 0)
            <table class="table table-bordered">
                <tr>
                    <td>Cím</td>
                    <td>Szerző</td>
                    <td>Szöveg</td>
                </tr>
                @foreach($newsList as $news)
                    <tr>
                        <td>{{ $news->title }}</td>
                        <td>{{ $news->creator }}</td>
                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                                Megtekintés
                            </button>
                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">{{ $news->title }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        {!! $news->description !!}
                                    </div>
                                </div>
                                </div>
                            </div>
                        </td>
                        <td class="row mx-0 justify-content-center">
                            <a href="{{ route('news.edit', $news->id) }}" class="btn btn-primary float-right mr-1 mb-1">Szerkesztés</a>
                            <form action="{{ route('news.destroy', $news->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button onclick="return confirm('Biztos, hogy törölni szeretnéd a {{ $news->title }} című hírt?');" type="submit" class="btn btn-danger mr-1">Törlés</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>

            @if(count($newsList) > 0)
                <div class="pagination">
                    <?php echo $newsList->render();  ?>
                </div>
            @endif

        @else
            <i>Nincs még hír felvéve</i>
        @endif

    </div>
</div>

@endsection
