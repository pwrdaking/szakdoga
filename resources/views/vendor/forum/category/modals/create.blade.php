@component('forum::modal-form')
    @slot('key', 'create-category')
    @slot('title', "Kategória létrehozása")
    @slot('route', Forum::route('category.store'))

    <div class="mb-3">
        <label for="title">Név</label>
        <input type="text" name="title" value="{{ old('title') }}" class="form-control">
    </div>
    <div class="mb-3 d-none">
        <label for="description">{{ trans('forum::general.description') }}</label>
        <input type="text" name="description" value="{{ old('description') }}" class="form-control">
    </div>
    <div class="mb-3 d-none">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="accepts_threads" id="accepts-threads" value="1" checked>
            <label class="form-check-label" for="accepts-threads">{{ trans('forum::categories.enable_threads') }}</label>
        </div>
    </div>
    <div class="mb-3 d-none">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="is_private" id="is-private" value="1" {{ old('is_private') ? 'checked' : '' }}>
            <label class="form-check-label" for="is-private">{{ trans('forum::categories.make_private') }}</label>
        </div>
    </div>
    @include ('forum::category.partials.inputs.color')

    @slot('actions')
        <button type="submit" class="btn btn-primary pull-right">Létrehozás</button>
    @endslot
@endcomponent
