@extends('layouts.app')

@section('content')

<div class="hero-image">
  <div class="hero-text">
    <h1>Csoda bölcsi</h1>
    <a class="btn btn-primary" href="/register">Jelentkezz most!</a>
  </div>
</div>

 <div class="container pt-3">

  <h3 class="mb-3">
    Aktualitásaink
  </h3>

    @foreach ($newsList as $news)
    <div class="jumbotron py-5">
        <h2>{{ $news->title }}</h2>
        <hr class="mb-1">

        <div class="d-flex justify-content-between mb-3 opacity-50">
            <span>{{ $news->creator }}</span>
            <span>{{ $news->title }}</span>
        </div>
        <div>
            {!! $news->description !!}
        </div>

    </div>
    @endforeach

    @if(count($newsList) > 0)
        <div class="pagination">
            <?php echo $newsList->render();  ?>
        </div>
    @endif
<div>

    <h3 class="mb-3">
      Elérhetőség
    </h3>
    <div class="row mx-0 pb-5">

      <div class="col-12 col-md-6">
        <div id="map"></div>
      </div>
      <div class="col-12 col-md-6">
        <p>Bármilyen kérdéssel keressenek minket bizalommal.</p>
        <p>
          Email: gati.gergo@gmail.com <br>
          Telefon: +36 20 155 5687 <br>
        </p>

        <p>
          Ha gyermekét íratná be azt az alábbi űrlap kitöltésével teheti meg: <br>
        </p>
        <a class="btn btn-primary" href="/register">Jelentkezés</a>


      </div>

    </div>

    </div>

 </div>

 <script>
        function initMap() {
        // The location of Uluru
        const coordinate = { lat: 46.181793, lng: 18.954306 };
        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map"), {
          zoom: 16,
          center: coordinate,
        });
        // The marker, positioned at Uluru
        const marker = new google.maps.Marker({
          position: coordinate,
          map: map,
        });
      }
  </script>

  <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCo4p1j4jfrlkpD5qq0qVPACB4b95aXQ8Q&callback=initMap&libraries=&v=weekly"
    async
  >
  </script>

@endsection
