<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Csoda bölcsi') }}</title>
    <link rel='shortcut icon' type='image/x-icon' href='/images/favicon.ico' />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" media="all" crossorigin="anonymous">

    @yield('styles')
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container pt-2">
        <div>
            <a class="navbar-brand" href="/">
                <img src="/images/sun.png" class="img-fluid" style="max-height: 25px;" alt="Logo, Sun">
                Csoda bölcsi
            </a>
        </div>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="register">Beíratkozás</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Elérhetőség</a>
                    </li>
                @endguest
                @auth
                @if(Auth::user()->type == 0)
                    <!-- Nevelő -->
                    <li class="nav-item">
                        <a class="nav-link" href="/news">Hírek</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/meal">Étkezés</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/product">Szolgáltatások</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/diary">Napló</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/group">Csoportok</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/teacher">Nevelők</a>
                    </li>
                @endif
                @if(Auth::user()->type == 1)
                    <!-- Szülő -->
                    <li class="nav-item">
                        <a class="nav-link" href="/child">Gyermek</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/meal">Étkezés</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/product">Szolgáltatások</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/invoice">Számlák</a>
                    </li>
                @endif
                @endauth
            </ul>
            @auth
            @if(Auth::user()->type == 1)
                <span class="nav-item">
                    <a class="nav-link text-white" href="/cart" >
                        <i class="fas fa-shopping-cart"></i>
                        <span class="badge badge-danger secondCartCount"> {{ Cart::count() ?? 0 }} </span>
                    </a>
                </span>
            @endif
            @endauth
             <span class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                  <i class="fas fa-user"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @guest
                    <a class="dropdown-item" href="/login">Bejelentkezés</a>
                    <a class="dropdown-item" href="/register">Beíratkozás</a>
                    @endguest
                    @auth
                        <a class="dropdown-item" href="/home">Admin felület</a>
                        <a class="dropdown-item" href="/profile">Profil</a>
                        <a class="dropdown-item" href="/forum">Fórum</a>

                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form-2').submit();">
                            Kijelentkezés
                        </a>
                        <form id="logout-form-2" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    @endauth
                </div>
              </span>
          </div>
        </div>
    </nav>

    <div id="app">
        @yield('content')
    </div>

</body>
</html>
