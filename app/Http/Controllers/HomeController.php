<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\PaymentSuccess;
use App\User;

class HomeController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('user.home');
    }

    public function test() {

        $products = [];

        $product1 = (object)[];
        $product1->name = "Fozs";
        $product1->qty = 3;
        $product1->price = "1000 Ft";
        $products[] = $product1;

        $product2 = (object)[];
        $product2->name = "Kaga";
        $product2->qty = 2;
        $product2->price = "1200 Ft";
        $products[] = $product2;


        $emailData = [
            'name' => "Nagy Árpi",
            'date' => "2021-12-18",
            'products' => $products,
            'grand_total' => 10000
        ];

        Mail::to('b2c51a8b30-bbdc7b@inbox.mailtrap.io')->send(new PaymentSuccess($emailData));
    }

}
