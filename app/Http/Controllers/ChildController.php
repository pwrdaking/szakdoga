<?php

namespace App\Http\Controllers;

use App\Absence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Child;
use App\Group;
use App\Message;
use App\Product;
use App\Subscription;
use App\User;
use Carbon\Carbon;

class ChildController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $children = Child::orderBy('id', 'DESC')->paginate(30);

        foreach ($children as $child) {
            if ($child->is_confirmed == 1) {
                $group = Group::find($child->group_id);
                $child->group = $group->name ?? "Folyamatban";
            } else {
                $child->group = "Folyamatban";
            }
        }

        return view('user.child.index')->withChildren($children);
    }

    public function show($id) {
        $child = Child::find($id);

        $group = Group::where('id', $child->group_id)->first();
        $child->group_name = $group->name ?? "Nincs";

        $child->messages = Message::where('child_id', $child->id)
                                    ->orderBy('created_at', 'ASC')
                                    ->get();

        $from = Carbon::createFromFormat('Y-m-d', $child->birthday);
        $to = Carbon::now()->addMonth();
        $child->months = $to->diffInMonths($from);

        $child->absences = Absence::where('child_id', $child->id)
            ->orderBy('end', 'DESC')
            ->get();

        $lastBaseSubscription = Subscription::where('child_id', $child->id)
            ->where('type', 0)
            ->orderBy('valid_until', 'DESC')->first();

        if ($lastBaseSubscription) {
            $lastValidSubDate = Carbon::createFromFormat('Y-m-d', $lastBaseSubscription->valid_until);
            $now = Carbon::now();
            if (!$now->gt($lastValidSubDate)) {
                $child->base_subscription_until = $lastValidSubDate->toDateString();
            }
        }

        $specialSubscriptions = Subscription::where('child_id', $child->id)
            ->where('type', 1)
            ->orderBy('valid_until', 'DESC')->get();

        $activeSpecialSubscriptions = [];
        $inactiveSpecialSubscriptions = [];
        foreach ($specialSubscriptions as $sub) {
            $subDate = Carbon::createFromFormat('Y-m-d', $sub->valid_until);
            $sub->product = Product::where('id', $sub->product_id)->first()->title;
            $now = Carbon::now();
            if ($now->gt($subDate)) {
                $inactiveSpecialSubscriptions[] = $sub;
            } else {
                $activeSpecialSubscriptions[] = $sub;
            }
        }

        if (Auth::user()->type == 0) {
            $child->parent = User::where('id', $child->parent_id)->first();
            return view('teacher.child.show')
                ->withChild($child)
                ->withActiveSpecialSubscriptions($activeSpecialSubscriptions)
                ->withInactiveSpecialSubscriptions($inactiveSpecialSubscriptions);;
        }

        if (Auth::user()->type == 1) {
            if (isset($group->teacher_id)) {
                if ($group->teacher_id != 0 && $group->teacher_id != -1) {
                    $child->teacher = User::where('id', $group->teacher_id)->first();
                }
            }
            return view('user.child.show')
                ->withChild($child)
                ->withActiveSpecialSubscriptions($activeSpecialSubscriptions)
                ->withInactiveSpecialSubscriptions($inactiveSpecialSubscriptions);
        }
    }

    public function create() {
        return view('user.child.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'child_name' => 'required'
        ]);

        $child = new Child;
        $child->child_name = $request->input('child_name');
        $child->sensitivity = $request->input('sensitivity');
        $child->base_disease = $request->input('base_disease');
        $child->birthday = $request->input('birthday');
        $child->parent_id = Auth::user()->id;
        $child->save();

        return redirect()->route('child.index');
    }

    public function edit($id) {
        return view('user.child.edit')->withChild(Child::find($id));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'child_name' => 'required'
        ]);

        $child = Child::find($id);
        $child->child_name = $request->input('child_name');
        $child->sensitivity = $request->input('sensitivity');
        $child->base_disease = $request->input('base_disease');
        $child->save();

        return redirect()->route('child.index');
    }

    public function destroy($id) {
        $child = Child::find($id);
        $child->delete();
        return redirect()->route('child.index');
    }

}
