<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Absence;

class AbsenceController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function create() {
        return view('user.absence.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'start' => 'required',
            'end' => 'required',
            'cause' => 'required',
        ]);

        $from = Carbon::createFromFormat('Y-m-d', $request->input('start'));
        $end = Carbon::createFromFormat('Y-m-d', $request->input('end'));
        $isValid = $end->gt($from);

        if (!$isValid) {
            return view('user.absence.create')
                    ->withErrorMessage("A kezdetnek nagyobbnak kell lenni mint a végnek");
        }

        $absence = new Absence();
        $absence->start = $request->input('start');
        $absence->end = $request->input('end');
        $absence->cause = $request->input('cause');
        $absence->child_id = $request->input('child_id');
        $absence->save();

        return redirect("/child/" . $absence->child_id);
    }

    public function edit($id) {
        return view('user.absence.edit')->withAbsence(Absence::find($id));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'start' => 'required',
            'end' => 'required',
            'cause' => 'required',
        ]);

        $from = Carbon::createFromFormat('Y-m-d', $request->input('start'));
        $end = Carbon::createFromFormat('Y-m-d', $request->input('end'));
        $isValid = $end->gt($from);

        if (!$isValid) {
            return view('user.absence.edit')
                    ->withAbsence(Absence::find($id))
                    ->withErrorMessage("A kezdetnek nagyobbnak kell lenni mint a végnek");
        }

        $absence = Absence::find($id);
        $absence->start = $request->input('start');
        $absence->end = $request->input('end');
        $absence->cause = $request->input('cause');
        $absence->child_id = $request->input('child_id');
        $absence->save();

        return redirect("/child/" . $absence->child_id);
    }

    public function destroy($id) {
        $absence = Absence::find($id);
        $child_id = $absence->child_id;
        $absence->delete();

        return redirect("/child/" . $child_id);
    }

}
