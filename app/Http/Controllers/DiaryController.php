<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Group;
use App\Absence;
use App\Child;
use App\Plan;

class DiaryController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $group = Group::where('teacher_id', Auth::user()->id)->first();
        $children = Child::where('group_id', $group->id)->get();

        $childrenIds = [];
        foreach ($children as $child) {
            $childrenIds[] = $child->id;
        }

        $plans = Plan::where('group_id', $group->id)
                        ->orderBy('week', 'DESC')
                        ->get();

        $absences = Absence::whereIn('child_id', $childrenIds)
                        ->orderBy('start', 'DESC')
                        ->get();

        foreach ($absences as $absence) {
            $absence->child_name = Child::where('id', $absence->child_id)->first()->child_name;
        }

        return view('teacher.diary.index')
                ->withGroup($group)
                ->withChildren($children)
                ->withAbsences($absences)
                ->withPlans($plans);
    }

}
