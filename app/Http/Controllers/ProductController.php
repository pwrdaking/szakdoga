<?php

namespace App\Http\Controllers;

use App\Child;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\Subscription;
use Carbon\Carbon;

class ProductController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $baseProducts = Product::where('type', 0)->get();
        $specialProducts = Product::where('type', 1)->get();

        if (Auth::user()->type == 0) {
            return view('teacher.product.index')
                        ->withBaseProducts($baseProducts)
                        ->withSpecialProducts($specialProducts);
        }

        if (Auth::user()->type == 1) {
            $children = Child::where('parent_id', Auth::user()->id)->get();
            return view('user.product.index')
                        ->withBaseProducts($baseProducts)
                        ->withSpecialProducts($specialProducts)
                        ->withChildren($children);
        }
    }

    public function create() {
        return view('teacher.product.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'validity_count' => 'required',
            'price' => 'required',
            'type' => 'required'
        ]);

        $product = new Product;
        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->validity_count = $request->input('validity_count');
        $product->price = $request->input('price');
        $product->type = $request->input('type');
        $product->save();

        return redirect()->route('product.index');
    }

    public function edit($id) {
        return view('teacher.product.edit')->withProduct(Product::find($id));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'validity_count' => 'required',
            'price' => 'required',
        ]);

        $product = Product::find($id);
        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->validity_count = $request->input('validity_count');
        $product->price = $request->input('price');
        $product->save();

        return redirect()->route('product.index');
    }

    public function destroy($id) {
        $product = Product::find($id);
        $product->delete();
        return redirect()->route('product.index');
    }

    public function stopSubscription($id) {
        $sub = Subscription::where('id', $id)->first();

        $now = Carbon::now()->toDateString();

        if ($sub) {
            $sub->valid_until = $now;
            $sub->save();
            return redirect('/child/' . $sub->child_id);
        }

        return redirect('/child');
    }
}
