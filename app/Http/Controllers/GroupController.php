<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\SignUpMail;
use App\User;
use App\Child;
use App\Group;

class GroupController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $unConfirmedChildren = Child::where('group_id', 0)
                                        ->orWhere('group_id', -1)
                                        ->get();

        $groups = Group::orderBy('id', 'DESC')->get();

        $isAlreadyHaveGroup = false;
        foreach ($groups as $group) {
            $group->isMine = false;
            $group->teacher = User::find($group->teacher_id)->parent_name;
            if ($group->teacher == Auth::user()->parent_name) {
                $isAlreadyHaveGroup = true;
                $group->isMine = true;
            }

            $group->children = Child::where('group_id', $group->id)
                                    ->get();
        }

        return view('teacher.group.index')
                ->withUnConfirmedChildren($unConfirmedChildren)
                ->withGroups($groups)
                ->withIsAlreadyHaveGroup($isAlreadyHaveGroup);
    }

    public function addChildren($id, Request $request) {
        $selectedGroupId = $request->input('group');
        $child = Child::find($id);
        $parent = User::find($child->parent_id);
        $group = Group::find($selectedGroupId);

        $emailData = [
            'parent_name' => $parent->parent_name,
            'child_name' => $child->child_name,
            'group_name' => $group->name
        ];

        Mail::to("b2c51a8b30-bbdc7b@inbox.mailtrap.io")->send(new SignUpMail($emailData));

        $child->group_id = $selectedGroupId;
        $child->is_confirmed = 1;
        $child->save();

        return redirect()->route('group.index');
    }

    public function removeChildren(Request $request) {
        $child = Child::find($request->input('child'));
        $child->group_id = -1;
        $child->save();

        return redirect()->route('group.index');
    }

    public function create() {
        $teachers = User::where('type', 0)->get();
        return view('teacher.group.create')->withTeachers($teachers);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $group = new Group;
        $group->name = $request->input('name');
        $group->teacher_id = $request->input('teacher');
        $group->save();

        return redirect()->route('group.index');
    }

    public function edit($id) {
        $teachers = User::where('type', 0)->get();
        return view('teacher.group.edit')
                    ->withGroup(Group::find($id))
                    ->withTeachers($teachers);;
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $group = Group::find($id);
        $group->name = $request->input('name');
        $group->teacher_id = $request->input('teacher');
        $group->save();
        return redirect()->route('group.index');
    }

    public function destroy($id) {
        $group = Group::find($id);

        $children = Child::where('group_id', $id)->get();
        foreach ($children as $child) {
            $child->group_id = -1;
            $child->save();
        }

        $group->delete();

        return redirect()->route('group.index');
    }

}
