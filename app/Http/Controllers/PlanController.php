<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;

class PlanController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function create() {
        return view('teacher.plan.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'week' => 'required',
            'body' => 'required'
        ]);

        $plan = new Plan;
        $plan->week = $request->input('week');
        $plan->body = $request->input('body');
        $plan->group_id = $request->input('group_id');
        $plan->save();

        return redirect()->route('diary.home');
    }

    public function edit($id) {
        return view('teacher.plan.edit')->withPlan(Plan::find($id));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'week' => 'required',
            'body' => 'required'
        ]);

        $plan = Plan::find($id);
        $plan->week = $request->input('week');
        $plan->body = $request->input('body');
        $plan->group_id = $request->input('group_id');
        $plan->save();
        return redirect()->route('diary.home');
    }

    public function destroy($id) {
        $plan = Plan::find($id);
        $plan->delete();
        return redirect()->route('diary.home');
    }
}
