<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\PaymentSuccess;
use Carbon\Carbon;
use App\Product;
use App\Order;
use App\Subscription;

class OrderController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function checkout() {
        $itemsToPay = array();

        foreach (Cart::content() as $item) {
            $tempItems = array(
                  'price_data' => [
                    'currency' => 'huf',
                    'product_data' => [
                      'name' => $item->name . " (" . $item->options->week . " hét) - (" . $item->options->childName . ")",
                    ],
                    'unit_amount' => $item->price * 100 *1.27,
                  ],
                  'quantity' => $item->qty,
            );

            array_push($itemsToPay, $tempItems);
        }

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $session = \Stripe\Checkout\Session::create ([
            'payment_method_types' => ['card'],
            'line_items' => [[$itemsToPay]],
            'mode' => 'payment',
            'success_url' => 'http://127.0.0.1:8000/payment-success',
            'cancel_url' => 'http://127.0.0.1:8000/cart?payment=false',
        ]);

        return redirect()->away($session->url);
    }

    public function invoice() {
        $orders = Order::where('user_id', Auth::user()->id)->get();

        return view('user.invoice.index')->withOrders($orders);
    }

    public function paymentSuccess() {
        $cartItems = Cart::content();
        $cartTotal = Cart::total(0, "", "");

        $items = "";
        $index = 0;
        foreach ($cartItems as $item) {
            $items = $items . ($index != 0 ? ", " : "") . "(" . $item->qty . " db) " . $item->name . " - (" . $item->options->childName . ")";
            $index++;
        }

        $order = new Order;
        $order->items = $items;
        $order->user_id =  Auth::user()->id;
        $order->price = $cartTotal;
        $order->save();

        foreach ($cartItems as $item) {
            $product = Product::where('id', $item->options->itemId)->first();
            $sub = new Subscription;

            $weeks = $item->qty * $product->validity_count;
            $sub->type = $product->type;
            $sub->child_id = $item->options->childId;
            $sub->product_id = $product->id;

            if ($sub->type == 0) {
                $lastValidSub = Subscription::where('child_id', $item->options->childId)->orderBy('valid_until', 'DESC')->first();

                if ($lastValidSub) {
                    $lastValidSubDate = Carbon::createFromFormat('Y-m-d', $lastValidSub->valid_until);
                    $now = Carbon::now();
                    if ($now->gt($lastValidSubDate)) {
                        $sub->valid_until = Carbon::now()->addWeeks($weeks)->toDateString();
                    } else {
                        $sub->valid_until = $lastValidSubDate->addWeeks($weeks)->toDateString();
                    }
                } else {
                    $sub->valid_until = Carbon::now()->addWeeks($weeks)->toDateString();
                }
            } else {
                $lastValidSub = Subscription::where('child_id', $item->options->childId)
                    ->where('product_id', $product->id)
                    ->orderBy('valid_until', 'DESC')->first();

                if ($lastValidSub) {
                    $lastValidSubDate = Carbon::createFromFormat('Y-m-d', $lastValidSub->valid_until);
                    $now = Carbon::now();
                    if ($now->gt($lastValidSubDate)) {
                        $sub->valid_until = Carbon::now()->addWeeks($weeks)->toDateString();
                    } else {
                        $sub->valid_until = $lastValidSubDate->addWeeks($weeks)->toDateString();
                    }
                } else {
                    $sub->valid_until = Carbon::now()->addWeeks($weeks)->toDateString();
                }
            }

            $sub->save();
        }

        $emailData = [
            'name' => Auth::user()->parent_name,
            'date' => Carbon::now()->toDateString(),
            'products' => $cartItems,
            'grand_total' => $cartTotal
        ];
        Mail::to("b2c51a8b30-bbdc7b@inbox.mailtrap.io")->send(new PaymentSuccess($emailData));

        Cart::destroy();

        return view('user.payment-success');
    }

}
