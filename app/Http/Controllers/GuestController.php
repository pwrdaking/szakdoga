<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class GuestController extends Controller
{
    public function index() {
        $news = News::orderBy('id', 'DESC')->paginate(30);

        return view('home')->withNewsList($news);
    }
}
