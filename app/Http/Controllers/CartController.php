<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Product;
use App\Child;

class CartController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $cartItems = Cart::content();
        $cartSubTotal = Cart::subtotal(0, "", " ");
        $cartTotal = Cart::total(0, "", " ");

        return view('user.cart.index')
                    ->withItems($cartItems)
                    ->withCartTotal($cartTotal)
                    ->withCartSubTotal($cartSubTotal);
    }

    public function add(Request $request) {
        $id = $request->id;
        $product = Product::where('id', $id)->first();
        $child = Child::where('id', $request->child)->first();

        $cartItems = Cart::content();
        foreach ($cartItems as $cartItem) {
            if ($cartItem->options->itemId == $id && $cartItem->options->childId == $child->id) {
                $newQuantity = $cartItem->qty + 1;
                Cart::update($cartItem->rowId, $newQuantity);
                return back()->withSuccess("Szolgáltatás sikeresen a kosárhoz adva!");
            }
        }

        $week = $product->validity_count;
        $price = $product->price;

        $options['itemId'] = $product->id;
        $options['description'] = $product->description;
        $options['week'] = $week;
        $options['childId'] = $child->id;
        $options['childName'] = $child->child_name;

        Cart::add($this->getNewUUID(), $product->title, 1, $price, $options);

        return back()->withSuccess("Szolgáltatás sikeresen a kosárhoz adva!");
    }

    public function deleteAll(){
        Cart::destroy();
        return back();
    }

    public function destroy(Request $request){
        Cart::remove($request->id);
        return back();
    }

    private function getNewUUID() {
        return (string) Str::uuid();
    }

}
