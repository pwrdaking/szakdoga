<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class TeacherController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $teachers = User::where('type', 0)
                            ->orderBy('id', 'DESC')->paginate(30);


        return view('user.teacher.index')->withTeachers($teachers);;
    }

    public function register() {
        return view('user.teacher.register');
    }

    public function destroy($id) {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('teacher');
    }

}
