<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\News;

class NewsController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $news = News::orderBy('id', 'DESC')->paginate(30);

        return view('teacher.news.index')->withNewsList($news);
    }

    public function create() {
        return view('teacher.news.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required'
        ]);

        $news = new News;
        $news->title = $request->input('title');
        $news->description = $request->input('description');
        $news->creator = Auth::user()->parent_name;
        $news->save();

        return redirect()->route('news.index');
    }

    public function edit($id) {
        return view('teacher.news.edit')->withNews(News::find($id));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required'
        ]);

        $news = News::find($id);
        $news->title = $request->input('title');
        $news->description = $request->input('description');
        $news->save();
        return redirect()->route('news.index');
    }

    public function destroy($id) {
        $news = News::find($id);
        $news->delete();
        return redirect()->route('news.index');
    }

}
