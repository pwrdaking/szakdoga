<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessageController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'message' => 'required'
        ]);

        $message = new Message;
        $message->message = $request->input('message');
        $message->child_id = $request->input('child_id');
        $message->save();

        return redirect("/child/" . $message->child_id);
    }

    public function destroy($id) {
        $message = Message::find($id);
        $message->delete();
        return redirect("/child/" . $message->child_id);
    }

}
