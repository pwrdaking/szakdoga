<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;

class MealController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $meals = Meal::orderBy('week', 'DESC')->get();

        return view('teacher.meal.index')->withMeals($meals);
    }

    public function create() {
        $daysEn = ['monday', 'tuesday', 'wednessday', 'thursday', 'friday'];
        $daysHu = ['Hétfő', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek'];

        return view('teacher.meal.create')
                ->withDaysEn($daysEn)
                ->withDaysHu($daysHu);
    }

    public function store(Request $request) {

        $meal = new Meal;
        $meal->week = $request->input('week');

        $meal->monday_breakfast = $request->input('monday-breakfast') ?? "-";
        $meal->monday_lunch = $request->input('monday-lunch') ?? "-";
        $meal->monday_dinner = $request->input('monday-dinner') ?? "-";

        $meal->tuesday_breakfast = $request->input('tuesday-breakfast') ?? "-";
        $meal->tuesday_lunch = $request->input('tuesday-lunch') ?? "-";
        $meal->tuesday_dinner = $request->input('tuesday-dinner') ?? "-";

        $meal->wednessday_breakfast = $request->input('wednessday-breakfast') ?? "-";
        $meal->wednessday_lunch = $request->input('wednessday-lunch') ?? "-";
        $meal->wednessday_dinner = $request->input('wednessday-dinner') ?? "-";

        $meal->thursday_breakfast = $request->input('thursday-breakfast') ?? "-";
        $meal->thursday_lunch = $request->input('thursday-lunch') ?? "-";
        $meal->thursday_dinner = $request->input('thursday-dinner') ?? "-";

        $meal->friday_breakfast = $request->input('friday-breakfast') ?? "-";
        $meal->friday_lunch = $request->input('friday-lunch') ?? "-";
        $meal->friday_dinner = $request->input('friday-dinner') ?? "-";

        $meal->save();

        return redirect()->route('meal.index');
    }

    public function destroy($id) {
        $meal = Meal::find($id);
        $meal->delete();
        return redirect()->route('meal.index');
    }

}
