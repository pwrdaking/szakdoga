<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\user;

class ProfileController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function edit() {
        return view('user.profile.edit')->withUser(Auth::user());
    }

    public function update(Request $request) {
            $this->validate($request, [
                'parent_name' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'min:10'],
                'email' => ['required', 'string', 'email', 'max:255']
            ]);

            if ($request->input('password') != null && $request->input('password_confirmation') != null) {
                $this->validate($request, [
                    'password' => ['required', 'string', 'min:8', 'confirmed']
                ]);
            }

            $user = User::find(Auth::user()->id);
            $user->parent_name = $request->input('parent_name');
            $user->phone = $request->input('phone');
            $user->email = $request->input('email');

            if ($request->input('password') != null && $request->input('password_confirmation') != null) {
                $user->password = Hash::make($request->input('password'));
            }


            $user->save();
            return redirect()->route('profile');
    }

}
